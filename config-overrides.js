const path = require('path')

module.exports = function (config) {
	config = {
		...config,
		entry: './src/index.tsx',
		resolve: {
			...config.resolve,
			extensions: ['.js', '.jsx', '.tsx', '.ts'],
			alias: {
				src: path.resolve(__dirname, 'src'),
			},
		},
	}

	return config
}
