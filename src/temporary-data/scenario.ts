import block1_img1 from '../resources/images/block1_img1.svg'

interface IScenario {
	title: string
	blocks: Array<any>
}

interface IContentBlock {
	title?: string
	image?: string
	text?: string
	something: any
}

export const exampleScenario: IScenario = {
	title: 'Язычник (по Джеку Лондону)',
	blocks: [
		{
			title: 'Отплытие',
			image: block1_img1,
		},
	],
}
