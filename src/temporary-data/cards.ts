import icon from 'src/resources/images/common-ava.jpg'
import { IFullCard } from '../interfaces/card'

export const cards: Array<IFullCard> = [
	{
		name: 'card1',
		path: 'card1',
		title: 'First Scene',
		description: 'Nice scene',
		icon,
	},

	{
		name: 'card2',
		path: 'card2',
		title: 'Second Scene',
		description: 'Feel your fear!',
		icon,
	},

	{
		name: 'card3',
		path: 'card3',
		title: 'Thirty Scene',
		description: 'Summer days',
		icon,
	},

	{
		name: 'card4',
		path: 'card4',
		title: 'Fourty Scene',
		description: 'Hobby time',
		icon,
	},

	{
		name: 'card5',
		path: 'card5',
		title: 'Fifty Scene',
		description: 'Fly away like the eagle!',
		icon,
	},
]
