import React from 'react'
import { AppBar } from '@material-ui/core'
import { ReactElement } from 'react'

interface IHeaderProps {
	children: ReactElement
}

export function Header({ children }: IHeaderProps): ReactElement {
	return <AppBar position="static">{children}</AppBar>
}
