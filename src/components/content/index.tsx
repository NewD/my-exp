import React, { ReactElement } from 'react'
import { Container } from '@material-ui/core'

interface IContantProps {
	children: ReactElement
}

export function Content({ children }: IContantProps): ReactElement {
	return <Container>{children}</Container>
}
