import React from 'react'
import { ReactElement } from 'react'
import { Content } from '../content'
import { Header } from '../header'

interface ILayout {
	header: ReactElement
	content: ReactElement
}

export function MainLayout({ header, content }: ILayout): ReactElement {
	return (
		<>
			<Header>{header}</Header>

			<Content>{content}</Content>
		</>
	)
}
