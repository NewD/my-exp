export interface IMessage {
	text: string
	author: string
	date: Date
}
