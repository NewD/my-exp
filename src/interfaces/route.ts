import { ReactElement } from 'react'

export interface IRoute {
	name: string
	path: string
	component: () => ReactElement
	routes?: Array<IRoute>
	exact: boolean
}
