export interface ICard {
	title: string
	description: string
	icon: string
}

export interface IFullCard extends ICard {
	path: string
	name: string
}
