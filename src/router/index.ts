import { mainRoutes } from "../modules/main-page";
import { sceneRoutes } from "../modules/scene";


export const routes = [...mainRoutes, ...sceneRoutes]
