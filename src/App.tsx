import React, { ReactElement } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { IRoute } from './interfaces/route'
import { routes } from './router'

function App() {
	return (
		<Router>
			<Switch>
				{routes.map(
					(route: IRoute): ReactElement => (
						<Route
							path={route.path}
							// render={props => (
							// 	// pass the sub-routes down to keep nesting
							// 	<route.component {...props} routes={route.routes} />
							// )}
							component={route.component}
							exact={route.exact}
							key={route.name}
						/>
					)
				)}
			</Switch>
		</Router>
	)
}

export default App
