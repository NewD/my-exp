import { MainLayout } from 'src/components/common-layout'
import { Content } from '../../components/content'

export function MainPage() {
	return (
		<>
			<MainLayout header="My nice app" content={<Content />} />
		</>
	)
}
