import { IRoute } from '../../interfaces/route'
import { MainPage } from './views/main'

export const mainRoutes: Array<IRoute> = [
	{
		name: 'main-page',
		path: '/',
		component: MainPage,
		exact: true,
	},
]
