const { makeStyles } = require('@material-ui/core')

export const cardStyle = makeStyles({
	card: {
		backgroundColor: 'lightGrey',
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},

	cardImage: {
		height: '260px',
		width: '100%',
	},
})
