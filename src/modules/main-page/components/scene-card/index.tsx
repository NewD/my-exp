import React from 'react'
import { Card, CardContent, CardHeader, CardMedia, Typography } from '@material-ui/core'
import { cardStyle } from './style'
import { ICard } from '../../../../interfaces/card'

export function SceneCard({ title, description, icon }: ICard) {
	const classes = cardStyle()
	return (
		<Card className={classes.card}>
			<CardHeader title={title} />
			<CardMedia className={classes.cardImage} image={icon} />
			<CardContent>
				<Typography>{description}</Typography>
			</CardContent>
		</Card>
	)
}
