import React from 'react'
import { cards } from '../../../../temporary-data/cards'
import { CardList } from '../cards-list'

export function Content() {
	return <CardList cards={cards} />
}
