import React from 'react'
import { Paper } from '@material-ui/core'
import { Link } from 'react-router-dom'
import { SceneCard } from '../scene-card'
import { cardListStyle } from './style'
import { IFullCard } from '../../../../interfaces/card'


interface ICardListProps {
	cards: Array<IFullCard>
}

export function CardList({ cards }: ICardListProps) {
	const classes = cardListStyle()

	return (
		<Paper className={classes.cardList}>
			{cards.map(item => (
				<Link className={classes.cardItem} to={item.path} key={item.name}>
					<SceneCard title={item.title} description={item.description} icon={item.icon} />
				</Link>
			))}
		</Paper>
	)
}
