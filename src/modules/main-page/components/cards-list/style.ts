const { makeStyles } = require('@material-ui/core')

export const cardListStyle = makeStyles({
	cardList: {
		width: '100%',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
		flexWrap: 'wrap',
	},

	cardItem: {
		width: '30%',
	},
})
