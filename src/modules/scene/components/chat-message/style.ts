import { makeStyles } from '@material-ui/core'

export const messageStyle = makeStyles({
	myMessage: {
		backgroundColor: 'lightblue',
	},

	alienMessage: {
		backgroundColor: 'lightred',
	},
})
