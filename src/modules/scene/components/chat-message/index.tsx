import React, { ReactElement } from 'react'
import { Typography } from '@material-ui/core'
import { messageStyle } from './style'

interface IProps {
	text: string
	isMy: boolean
	date: Date
}

export function ChatMessage({ text, isMy, date }: IProps): ReactElement {
	const classes = messageStyle()

	return (
		<div className={isMy ? classes.myMessage : classes.alienMessage}>
			<Typography variant="caption">{date.toString()}</Typography>
			<Typography variant="body1">{text}</Typography>
		</div>
	)
}
