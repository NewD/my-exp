import React from 'react'
import { Box } from '@material-ui/core'
import { ChatMessage } from '../chat-message'
import { IMessage } from '../../../../interfaces/message'

interface IProps {
	messages: Array<IMessage>
}

export function ChatBody({ messages }: IProps) {
	return (
		<Box>
			{messages.map(message => {
				const isMy = message.author === 'newd' ? true : false

				return <ChatMessage isMy={isMy} text={message.text} date={message.date} />
			})}
		</Box>
	)
}
