import { IRoute } from '../../interfaces/route'
import { ScenePage } from './views/main'

export const sceneRoutes: Array<IRoute> = [
	{
		name: 'scene',
		path: '/:scene',
		component: ScenePage,
		exact: false,
	},
]
