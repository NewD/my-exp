import React, { ReactElement } from 'react'
import { Paper, Typography } from '@material-ui/core'

export function ScenePage(): ReactElement {
	return (
		<Paper>
			<Typography variant="h4">SCENE PAGE</Typography>
		</Paper>
	)
}
